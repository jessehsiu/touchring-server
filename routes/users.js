var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('userstudy', { title: 'Express' });
});

router.get('/control', function(req, res, next) {
  res.render('userstudyControl', { title: 'Express' });
});

module.exports = router;
