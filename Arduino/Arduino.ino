#include "Adafruit_MPR121.h"
#define _BV(bit) (1 << (bit))

// your network name also called SSID
char ssid[] = "HomeNet";
// your network password
char password[] = "ntpu49985052";

#if defined(ARDUINO) 
SYSTEM_MODE(MANUAL);//do not connect to cloud
#else
SYSTEM_MODE(AUTOMATIC);//connect to cloud
#endif

uint16_t port = 5000;     // port number of the server
IPAddress server(192, 168, 31, 163);   // IP Address of the server
// TCPClient client;
UDP Udp;
unsigned int localPort = 2390; 

void printWifiStatus();

// MPR 121 Sensors.
Adafruit_MPR121 cap_test = Adafruit_MPR121();

void setup() {
  //Initialize serial and wait for port to open:
  Serial.begin(115200);

  if (!cap_test.begin(0x5A)) {
    Serial.println("MPR121 not found, check wiring?");
    while (1);
  }

  // attempt to connect to Wifi network:
  Serial.print("Attempting to connect to Network named: ");
  // print the network name (SSID);
  Serial.println(ssid); 
  // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
  WiFi.on();
  WiFi.setCredentials(ssid, password);
  WiFi.connect();

  while (WiFi.connecting()) {
    // print dots while we wait to connect
    Serial.print(".");
    delay(300);
  }
  
  Serial.println("\nYou're connected to the network");
  Serial.println("Waiting for an ip address");
  
  IPAddress localIP = WiFi.localIP();

  while (localIP[0] == 0)
  {
      localIP = WiFi.localIP();
      Serial.println("waiting for an IP address");
      delay(1000);
  }

  Serial.println("\nIP Address obtained");
  
  // you're connected now, so print out the status  
  printWifiStatus();
  Udp.begin(localPort);
  // attempt to connect to the server
  // Serial.println("Attempting to connect to server");

  // uint8_t tries = 0;
  // while (client.connect(server, port) == false) {
  //   Serial.print(".");
  //   if (tries++ > 100) {
  //     Serial.println("\nThe server isn't responding");
  //     while(1);
  //   }
  //   delay(100);
  // }
  
  // //we've connected to the server by this point
  // Serial.println("\nConnected to the server!");
}

// Sending String
String sendingString = "";

// For remember the current touched pads
uint16_t currtouched_test = 0;
// int sensors = 48;
// int dataInASensor = 5;
uint8_t buffer[240];
int position = 0;
void loop() {
  position = 0;
  // Clear String
  // sendingString = "[";

  Serial.print((uint8_t)cap_test.filteredData(0));
  Serial.print(",");
  Serial.println((uint8_t)cap_test.baselineData(0));
  // // Sensing touches.
  currtouched_test = cap_test.touched();
  for(int i=0; i<12; i++){
    bool isTouched = currtouched_test & _BV(i);
    uint16_t filter = cap_test.filteredData(i);
    buffer[position++] = filter & 0xff;
    buffer[position++] = (filter >> 8);
    uint16_t base = cap_test.baselineData(i);
    buffer[position++] = base & 0xff;
    buffer[position++] = (base >> 8);
    buffer[position++] = isTouched;
  }
  for(int i=0; i<12; i++){
    bool isTouched = currtouched_test & _BV(i);
    uint16_t filter = cap_test.filteredData(i);
    buffer[position++] = filter & 0xff;
    buffer[position++] = (filter >> 8);
    uint16_t base = cap_test.baselineData(i);
    buffer[position++] = base & 0xff;
    buffer[position++] = (base >> 8);
    buffer[position++] = isTouched;
  }
  for(int i=0; i<12; i++){
    bool isTouched = currtouched_test & _BV(i);
    uint16_t filter = cap_test.filteredData(i);
    buffer[position++] = filter & 0xff;
    buffer[position++] = (filter >> 8);
    uint16_t base = cap_test.baselineData(i);
    buffer[position++] = base & 0xff;
    buffer[position++] = (base >> 8);
    buffer[position++] = isTouched;
  }
  for(int i=0; i<12; i++){
    bool isTouched = currtouched_test & _BV(i);
    uint16_t filter = cap_test.filteredData(i);
    buffer[position++] = filter & 0xff;
    buffer[position++] = (filter >> 8);
    uint16_t base = cap_test.baselineData(i);
    buffer[position++] = base & 0xff;
    buffer[position++] = (base >> 8);
    buffer[position++] = isTouched;
  }
  
  Udp.beginPacket(server, port);
  Udp.write( buffer, sizeof(buffer)/sizeof(uint8_t));
  Udp.endPacket();
  delay(100);
  
}

void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("Network Name: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
