var TIMEFOREACHDOT = 6;


var QueryString = function () {
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = decodeURIComponent(pair[1]);
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
      query_string[pair[0]] = arr;
    } else {
      query_string[pair[0]].push(decodeURIComponent(pair[1]));
    }
  } 
    return query_string;
}();

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

Array.prototype.extend = function (other_array) {
    other_array.forEach(function(v) {this.push(v)}, this);    
}


var eachElements = [];
var finalElements = [];

// 150
// 140 ~ 290
/*120 ~ 160*/

var initWidth = 110;
var initHeight = 110;

var height = initHeight;
var width = initWidth;

var stepHeight = 50 / (parseInt(QueryString.row)+1);
var stepWidth = 180 / (parseInt(QueryString.col)+1);
var count = 0;
for (var j = 0; j < QueryString.row; j++) {
	height += stepHeight;
	console.log("hello")
	for (var i = 0; i < QueryString.col; i++) {
		width += stepWidth;
		$(".handImg").append('<div style="position:fixed;margin-top:'+ height +'px;margin-left:'+ width +'px;"><i id='+ count + ' class="fa fa-circle fa-1 dot"></i></div>');
		count++;
	}
	width = initWidth;
	
}

for (var i = 0; i < parseInt(QueryString.row) * parseInt(QueryString.col); i++) {
	eachElements.push(i);
}

for (var i = 0; i < TIMEFOREACHDOT; i++) {
	finalElements.extend(eachElements);
}
finalElements = shuffle(finalElements)

var currentTest = 0;

$("#" + finalElements[currentTest]).addClass('dot_acitve')
$("#progress").data({
	value: 1,
	total: finalElements.length
})
$(".nextBtn").click(function(event) {
	next()
});
$('.submit.button').click(function(event) {
	url = "http://127.0.0.1:3000/userstudy?row=" + $("#row").val() + "&col=" + $("#col").val();
	window.location.replace(url);
});
var socket = io('http://localhost:3000');


socket.on('refreshImage', function (data) {

	$(".dot").each(function (index) {
		$(this).removeClass('dot_acitve');
	});

	$('#progress').progress('increment')
	
	currentTest ++;
	$("#" + finalElements[currentTest]).addClass('dot_acitve')
	$('#count').text("Current: "+ currentTest)

	if (currentTest == finalElements.length)
	{
		$(".green.message").show()
		$(".blue.message").show()
		$('#progress').hide()
		$(".blue.message").text(finalElements)
		$(".nextBtn").hide()
	}
})

function next() {
	
	socket.emit('shootImage');
}

window.onkeydown = function(e) {
  if (e.keyCode == 32 && e.target == document.body) {
    e.preventDefault();
    next();
  }
};

// $(document).keyup(function(evt) {
// if (evt.keyCode == 32) {
// 	
// 	return false;
// }
// })

