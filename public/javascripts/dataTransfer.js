var currentSetting;
var nameArray = [3,1,12,6,4,14,9,7,17,10,2,11,15,5,13,18,8,16]
$('.fa-circle').each(function( index ) {
	$(this).attr('id', 'B' + index);//nameArray[index]
});
$('.ui.checkbox').checkbox();
$('.ui.dropdown').dropdown();

$('#savingBtn').click(function () {

	if (currentSetting.recording)
	{
		$(this).removeClass('negative')
		$(this).addClass('primary')
		$(this).text('Start Recording');

		currentSetting = {
			filename: $('#fileName').val(),
			currentType: $('#currentType').val(),
			filterCheck: $('#filterCheck').prop("checked"),
			baseCheck: $('#baseCheck').prop("checked"),
			stateCheck: $('#stateCheck').prop("checked"),
			recording: false
		}
		socket.emit('webSetting', currentSetting);
	}
	else{
		$(this).removeClass('primary')
		$(this).addClass('negative')
		$(this).text('Recording');

		currentSetting = {
			filename: $('#fileName').val(),
			currentType: $('#currentType').val(),
			filterCheck: $('#filterCheck').prop("checked"),
			baseCheck: $('#baseCheck').prop("checked"),
			stateCheck: $('#stateCheck').prop("checked"),
			recording: true
		}
		socket.emit('webSetting', currentSetting);
	}
})

var count = 0;
var socket = io('http://localhost:3000');
socket.on('data', function (data) {
	$('.fa-circle').css({color: 'black'});
	var value = data.data;
	for (var i = 0; i < value.length; i++) {
		if(value[i][2] ==1 )
		{
			$('#B'+i).css({color: 'red'});
		}
	}
});

socket.on('webSetting', function (data) {
	console.log(data);
	if (!data) {return}
	$('#fileName').val(data.filename);
	$("#currentType").dropdown('set value', data.currentType)
	$('#filterCheck').prop('checked', data.filterCheck);
	$('#baseCheck').prop('checked', data.baseCheck);
	$('#stateCheck').prop('checked', data.stateCheck);
	if (data.recording)
	{
		if ($('#savingBtn').hasClass('primary'))
		{
			$('#savingBtn').removeClass('primary')
			$('#savingBtn').addClass('negative')
			$('#savingBtn').text('Recording');
		}
	}
	else{
		if (!$('#savingBtn').hasClass('primary'))
		{
			$('#savingBtn').removeClass('negative')
			$('#savingBtn').addClass('primary')
			$('#savingBtn').text('Start Recording');
		}
	}
	currentSetting = data;
})