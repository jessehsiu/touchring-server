


var socket = io('http://localhost:3000');
socket.on('refreshImage', function (data) {
	$("#fileName").text(data)
	$('.image').attr('src', data)
});

function captureImage() {
	socket.emit('shootImage');
	if ($("#resetBtn").hasClass('disabled')) {
		$("#resetBtn").removeClass('disabled');
	}
}


$(".button.negative").click(function () {
	captureImage();
})
$("#resetBtn").click(function () {
	console.log('reset')
	socket.emit('resetCaptureCount');
	$("#resetBtn").addClass('disabled')
})

window.onkeydown = function(e) {
  if (e.keyCode == 32 && e.target == document.body) {
    e.preventDefault();
    captureImage();
  }
};