var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();
var http = require('http');

var port = 3000;
app.set('port', port);
var server = http.createServer(app);
server.listen(port);

var io = require('socket.io')(server);

var dgram = require('dgram');
var udpServer = dgram.createSocket('udp4');
var fs = require('fs');

// For Image Capture.
var gphoto2 = require('gphoto2');
var GPhoto = new gphoto2.GPhoto2();
var camera = null;
var shootingCount = 0;
GPhoto.list(function (list) {
  if (list.length === 0) return;
  camera = list[0];
  console.log('Found', camera.model);
  camera.getConfig(function (er, settings) {
  	console.log(er)
    console.log(settings);
  });

  // Set configuration values
  camera.setConfigValue('capturetarget', 1, function (er) {
    // console.log(er)
  });
});


var count = 0;
udpServer.on('listening', function () {
    var address = udpServer.address();
    console.log('UDP Server listening on ' + address.address + ":" + address.port);
});

// Message From Hardware
udpServer.on('message', function (message, remote) {
  var data = new Uint8Array(message);
  var sendJson = [];
  var tmp_filter = 0;
  var tmp_base = 0;
  for (var i = 0; i < data.length; i++) {
    switch(i%5) 
    {
      case 0:
        tmp_filter = (data[i+1] << 8) + data[i];
        i++;
        break;
      case 2:
        tmp_base = (data[i+1] << 8) + data[i];
        i++;
        break;
      case 4:
        sendJson.push([tmp_filter,tmp_base,data[i]])
        break;
    }
    
  }
  SocketIOclients.forEach(function (socket) {
    socket.emit('data', {data:sendJson})
  })
  // Saving data
  if (recordingMgr.isRecording())
  {
    recordingMgr.saveData(sendJson);
  }

});
// 192.168.31.163
udpServer.bind(5000, '10.5.2.255');

var recordingMgr = {
  jsondata: "",
  storeData: {},
  recording: false,
  filePath: null,
  isRecording: function () {
    return this.recording;
  },
  start: function () {
    this.filePath = './'+currentSetting.filename;
    var fileState = fs.existsSync(this.filePath);
    if (fileState)
    {
      var json = JSON.parse(fs.readFileSync(this.filePath,'utf8'));
      this.jsondata = json;
    }
    else{
      fs.writeFileSync(this.filePath, "[]")
    }

    this.storeData['currentType'] = currentSetting.currentType
    this.storeData['data'] = []
    this.recording = true;
  },
  saveData: function (data) {
    var recordingData = []
    var filter = currentSetting.filterCheck;
    var base = currentSetting.baseCheck;
    var state = currentSetting.stateCheck;
    for (var i = 0; i < data.length; i++) {
      var instanceData = [];
      if (filter)
      {
        instanceData.push(data[i][0])
      }
      if (base)
      {
        instanceData.push(data[i][1])
      }
      if (state)
      {
        instanceData.push(data[i][2])
      }
      recordingData.push(instanceData);
    }
    this.storeData['data'].push(recordingData)
  },
  stop: function () {
    this.recording = false;

    this.jsondata.push(this.storeData);
    fs.writeFileSync(this.filePath, JSON.stringify(this.jsondata));
    this.storeData = {}
  }
}



// ======= Web Client Transfer by socket.io =======

var currentSetting = {
    filename: "output.json",
    currentType: "",
    filterCheck: true,
    baseCheck: true,
    stateCheck: true,
    recording: false
  };
var SocketIOclients = [];
io.on('connection', function (socket) {
  console.log("Socket.IO newClient")
  SocketIOclients.push(socket);
  socket.emit('webSetting', currentSetting);
  socket.on('webSetting', function (msg) {
    io.sockets.emit('webSetting', msg);
    currentSetting = msg;
    if (currentSetting.recording)
    {
      recordingMgr.start();
    }
    else{
      recordingMgr.stop(); 
    }
  });

  socket.on('resetCaptureCount',function () {
    shootingCount = 0;
  });

  socket.on('shootImage', function () {
    if (camera != null)
    {
    	console.log("Shoot");
      

	 camera.takePicture({download: true}, function (er, data) {
		fs.writeFileSync('./public/CameraImage/picture'+ shootingCount +'.jpg', data);
		io.sockets.emit('refreshImage', '/CameraImage/picture'+ shootingCount +'.jpg');
		shootingCount++;
	});
    }

  });

  socket.on('disconnect', function () {
    SocketIOclients.splice(SocketIOclients.indexOf(socket), 1);
  });
});
// ======= Web Client Transfer by socket.io END =======



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/userstudy', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
